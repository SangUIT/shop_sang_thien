﻿using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;
using OnlineShopping.API.Model.Dtos;
using OnlineShopping.API.Model.RequestCommands;
using OnlineShopping.Services.Accounts;

namespace OnlineShopping.API.Client.Clients
{
    public class Accounts : IAccounts
    {
        private readonly HttpClient _client;

        public Accounts()
        {
            _client = ConfigClent.CreateHttpClient();
        }


        public async Task<AccountDto> GetUserByName(string username)
        {
            var account = new AccountDto();
            string url = $@"https://localhost:44374/api/Accounts/getbyname?name={username}";
            var response = await _client.GetAsync(url);
            var jsonResponse = await response.Content.ReadAsStringAsync();

            if (response.StatusCode == HttpStatusCode.OK)
            {
                account = Newtonsoft.Json.JsonConvert.DeserializeObject<AccountDto>(jsonResponse);
            }

            return account;
        }

        public async Task<LoginDto> Login(string username, string password)
        {
            var account = new LoginDto();
            string url = $@"https://localhost:44374/api/Accounts/Login";
            var stringContent = new StringContent(
                JsonConvert.SerializeObject(
                    new LoginRequest()
                    {
                        Username = username,
                        Password = password
                    }),
                Encoding.UTF8, "application/json");

            var response = await _client.PostAsync(url, stringContent);
            var jsonResponse = await response.Content.ReadAsStringAsync();

            if (response.StatusCode == HttpStatusCode.OK)
            {
                account = Newtonsoft.Json.JsonConvert.DeserializeObject<LoginDto>(jsonResponse);
            }

            return account;
        }
        //cai này la register nha không cần truyền role với token nha 
        public async Task<RegisterDto> Register(
            string username, string password, string Email, string[] roles = null, string token = null)
        {
            var result = new RegisterDto();
            string url = $@"https://localhost:44374/api/Accounts/Create";
            var stringContent = new StringContent(
                JsonConvert.SerializeObject(
                    new CreateAccountRequest()
                    {
                        Username = username,
                        Password = password,
                        Roles = roles,
                        Email = Email,
                        Token = token
                    }),
                Encoding.UTF8, "application/json");

            var response = await _client.PostAsync(url, stringContent);
            var jsonResponse = await response.Content.ReadAsStringAsync();

            if (response.StatusCode == HttpStatusCode.OK)
            {
                var jsonConvert = Newtonsoft.Json.JsonConvert.DeserializeObject<OperationResult<RegisterDto>>(jsonResponse);
                if(jsonConvert.IsSuccess)
                {
                    result = jsonConvert.Entity;
                }
            }

            return result;
        }
    }
}
