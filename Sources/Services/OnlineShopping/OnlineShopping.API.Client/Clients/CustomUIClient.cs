﻿using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using OnlineShopping.API.Model.Dtos;

namespace OnlineShopping.API.Client.Clients
{
    public class CustomUIClient : ICustomUIClient
    {
        private readonly HttpClient _client;
        public CustomUIClient()
        {
            _client = ConfigClent.CreateHttpClient();
        }
        public async Task<NavigationMenuDto> GetAllAsync()
        {
            var menus = new NavigationMenuDto();
            string url = $@"https://localhost:44374/api/CustomUI/getall";
            var response = await _client.GetAsync(url);
            var jsonResponse = await response.Content.ReadAsStringAsync();

            if (response.StatusCode == HttpStatusCode.OK)
            {
                menus = Newtonsoft.Json.JsonConvert.DeserializeObject<NavigationMenuDto>(jsonResponse);
            }

            return menus;
        }
    }
}
