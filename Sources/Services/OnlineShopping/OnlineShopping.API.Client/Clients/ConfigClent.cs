﻿using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;

namespace OnlineShopping.API.Client.Clients
{
    public static class ConfigClent
    {
        public static HttpClient CreateHttpClient()
        {
            var handler = new HttpClientHandler();
            var result = new HttpClient(handler);
            result.DefaultRequestHeaders.Clear();
            result.DefaultRequestHeaders.Add("soapaction", "api");
            result.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

            return result;
        }
    }
}
