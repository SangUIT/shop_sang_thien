﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using OnlineShopping.API.Model.Dtos;
using OnlineShopping.Services.Accounts;

namespace OnlineShopping.API.Client.Clients
{
    public interface IAccounts
    {
        Task<AccountDto> GetUserByName(string username);
        Task<LoginDto> Login(string username, string password);
        Task<RegisterDto> Register(string username, string password, string Email, string[] roles = null, string token = "");
    }
}
