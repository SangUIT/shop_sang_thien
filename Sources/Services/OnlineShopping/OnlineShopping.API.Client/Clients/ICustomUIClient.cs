﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using OnlineShopping.API.Model.Dtos;

namespace OnlineShopping.API.Client.Clients
{
    public interface ICustomUIClient
    {
        Task<NavigationMenuDto> GetAllAsync();
    }
}
