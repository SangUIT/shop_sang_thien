﻿using System;
using System.Collections.Generic;
using System.Net;
using System.Text;

namespace OnlineShopping.API.Client
{
    public class BaseJsonBridgeResponse
    {
        public HttpStatusCode StatusCode { get; set; }

        public string JsonResult { get; set; }
    }
}
