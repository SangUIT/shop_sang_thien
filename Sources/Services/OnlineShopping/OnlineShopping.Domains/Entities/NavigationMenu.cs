﻿using System;
using System.Collections.Generic;
using System.Text;

namespace OnlineShopping.API.Domains.Entities
{
    public class NavigationMenu : BaseEntity
    {
        public string Name { get; set; }
    }
}
