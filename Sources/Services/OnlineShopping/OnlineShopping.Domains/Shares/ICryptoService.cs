﻿using System;
using System.Collections.Generic;
using System.Text;

namespace OnlineShopping.API.Domains.Shares
{
    public interface ICryptoService
    {
        string GenerateSalt();
        string EncryptPassword(string password, string salt);
    }
}
