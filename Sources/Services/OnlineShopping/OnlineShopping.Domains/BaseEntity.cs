﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace OnlineShopping.API.Domains
{
    [Serializable]
    public abstract class BaseEntity
    {
        [Key]
        public virtual int ID { get; set; }
    }
}
