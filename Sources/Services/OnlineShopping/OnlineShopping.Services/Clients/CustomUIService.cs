﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OnlineShopping.API.Domains.Entities;
using OnlineShopping.API.Model.Dtos;
using OnlineShopping.Data;

namespace OnlineShopping.Services.Clients
{
    public class CustomUIService : ICustomUIService
    {
        private readonly IRepository _repository;

        public CustomUIService(IRepository repository)
        {
            _repository = repository;
        }
        public async Task<IEnumerable<NavigationMenuDto>> GetAllMenu()
        {
            var menus = await _repository.GetAllAsync<NavigationMenu>();

            if (menus == null)
            {
                return new List<NavigationMenuDto>();
            }

            var menusDto = menus.Select(x => new NavigationMenuDto()
            {
                Id = x.ID,
                Name = x.Name
            });

            return menusDto;


        }
    }
}
