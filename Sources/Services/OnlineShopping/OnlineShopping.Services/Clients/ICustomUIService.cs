﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OnlineShopping.API.Domains.Entities;
using OnlineShopping.API.Model.Dtos;

namespace OnlineShopping.Services.Clients
{
    public interface ICustomUIService
    {
        Task<IEnumerable<NavigationMenuDto>> GetAllMenu();
    }
}
