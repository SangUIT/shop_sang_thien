﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Principal;
using System.Text;
using System.Threading.Tasks;
using OnlineShopping.API.Domains.Entities;
using OnlineShopping.API.Domains.Shares;
using OnlineShopping.API.Model.Dtos;
using OnlineShopping.API.Model.RequestCommands;
using OnlineShopping.API.Model.Validation;
using OnlineShopping.Data;

namespace OnlineShopping.Services.Accounts
{
    public class AccountService : IAccountService
    {
        private readonly IRepository _repository;
        private readonly ICryptoService _cryptoService;

        public AccountService(IRepository repository, ICryptoService cryptoService)
        {
            _repository = repository;
            _cryptoService = cryptoService;
        }

        public async Task<OperationResult<RegisterDto>> CreateOnlyUser(CreateAccountRequest requestCommand)
        {
            return await CreateUserWithRoles(requestCommand);
        }

        public Task<OperationResult<RegisterDto>> CreateUserWithRole(CreateAccountRequest requestCommand)
        {
            return CreateUserWithRoles(requestCommand);
        }

        public async Task<OperationResult<RegisterDto>> CreateUserWithRoles(CreateAccountRequest requestCommand)
        {
            var users = await _repository.GetAllAsync<User>();
            var existingUser = users.FirstOrDefault(x => string
            .Equals(x.Name, requestCommand.Username, StringComparison.OrdinalIgnoreCase));

            if (existingUser != null)
            {
                return new OperationResult<RegisterDto>(false);
            }

            var passwordSalt = _cryptoService.GenerateSalt();

            var user = new User()
            {
                Name = requestCommand.Username,
                Salt = passwordSalt,
                Email = requestCommand.Email,
                IsLocked = false,
                Token = requestCommand.Token,
                HashedPassword = _cryptoService.EncryptPassword(requestCommand.Password, passwordSalt),
                CreatedOn = DateTime.Now
            };

            await _repository.CreateAsync<User>(user);
            await _repository.SaveAsync();

            if (requestCommand.Roles != null && requestCommand.Roles.Length > 0)
            {
                foreach (var roleName in requestCommand.Roles)
                {
                    await addUserToRole(user, roleName);
                }
            }

            var reUser = await GetUserWithRoles(user);

            if(reUser == null)
            {
                return new OperationResult<RegisterDto>(false);
            }

            return new OperationResult<RegisterDto>(true)
            {
                Entity = new RegisterDto()
                {
                    Email = reUser.User.Email,
                    Password = reUser.User.HashedPassword,
                    Token = reUser.User.Token,
                    Username = reUser.User.Name
                }
            };
        }

        public async Task<UserWithRoles> GetUser(int id)
        {
            var user = await _repository.GetByIdAsync<User>(id);
            return await GetUserWithRoles(user);
        }

        public async Task<UserWithRoles> GetUser(string name)
        {
            var user = await GetUserByName(name);
            return await GetUserWithRoles(user);
        }

        public async Task<bool> AddToRole(int userKey, string role)
        {
            var user = await _repository.GetByIdAsync<User>(userKey);

            if (user != null)
            {
                await addUserToRole(user, role);
                return true;
            }

            return false;
        }

        public Task<bool> AddToRole(string username, string role)
        {
            throw new NotImplementedException();

        }

        public async Task<bool> ChangePassword(string username, string oldPassword, string newPassword)
        {
            var users = await _repository.GetAllAsync<User>();
            var user = users.FirstOrDefault(x => string
            .Equals(x.Name, username.Trim(), StringComparison.OrdinalIgnoreCase));

            if (user != null && isPasswordValid(user, oldPassword))
            {
                user.HashedPassword =
                    _cryptoService.EncryptPassword(newPassword, user.Salt);

                await _repository.SaveAsync();
                return true;
            }

            return false;
        }
        public Task<IEnumerable<Role>> GetRoles()
        {
            throw new NotImplementedException();
        }


        public Task<bool> RemoveFromRole(string username, string role)
        {
            throw new NotImplementedException();
        }

        public Task<UserWithRoles> UpdateUser(User user, string username, string email)
        {
            throw new NotImplementedException();
        }
        public async Task<Role> GetRole(int key)
        {
            return await _repository.GetByIdAsync<Role>(key);
        }

        public async Task<ValidUserContext> ValidateUser(string username, string password)
        {
            var userCtx = new ValidUserContext();
            var user = await GetUserByName(username);

            if (user != null && isUserValid(user, password))
            {
                var userRoles = await GetUserRoles(user.ID);
                userCtx.UserWithRoles = new UserWithRoles()
                {
                    User = user,
                    Roles = userRoles.ToList()
                };
                var identity = new GenericIdentity(user.Name);
                userCtx.Principal = new GenericPrincipal(
                    identity,
                    userRoles.Select(x => x.Name).ToArray());
            }
            return userCtx;
        }

        //Private helper

        private bool isUserValid(User user, string password)
        {
            if (isPasswordValid(user, password))
            {
                return !user.IsLocked;
            }
            return false;
        }

        private async Task<User> GetUserByName(string name)
        {
            var users = await _repository.GetAllAsync<User>();
            return users.FirstOrDefault(x => string
            .Equals(x.Name, name, StringComparison.OrdinalIgnoreCase));
        }

        private bool isPasswordValid(User user, string password)
        {
            return string.Equals(
                    _cryptoService.EncryptPassword(
                        password, user.Salt), user.HashedPassword);
        }

        private async Task addUserToRole(User user, string roleName)
        {
            var roles = await _repository.GetAllAsync<Role>();
            var roleByName = roles.FirstOrDefault(x => string.Equals(x.Name, roleName));

            if (roleByName == null)
            {
                var tempRole = new Role()
                {
                    Name = roleName
                };

                await _repository.CreateAsync<Role>(tempRole);
                await _repository.SaveAsync();
                roleByName = tempRole;
            }

            var userInRole = new UserInRole()
            {
                RoleId = roleByName.ID,
                UserId = user.ID
            };

            await _repository.CreateAsync<UserInRole>(userInRole);
            await _repository.SaveAsync();
        }

        private async Task<IEnumerable<Role>> GetUserRoles(int id)
        {
            var userInRoles = await _repository.GetAllAsync<UserInRole>();
            userInRoles = userInRoles.Where(x => x.UserId == id);

            if (userInRoles != null && userInRoles.Count() > 0)
            {
                var userRoleKeys = userInRoles.Select(
                    x => x.RoleId).ToArray();

                var userRoles = await _repository.GetAllAsync<Role>();
                userRoles = userRoles.Where(x => userRoleKeys.Contains(x.ID));

                return userRoles;
            }

            return Enumerable.Empty<Role>();
        }

        private async Task<UserWithRoles> GetUserWithRoles(User user)
        {
            if (user != null)
            {
                var userRoles = await GetUserRoles(user.ID);

                return new UserWithRoles()
                {
                    User = user,
                    Roles = userRoles.ToList()
                };
            }

            return new UserWithRoles();
        }

        public async Task<LoginDto> LoginAsync(LoginRequest request)
        {
            var loginDto = new LoginDto();
            var userByName = await GetUserByName(request.Username);

            if (userByName == null)
            {
                loginDto.IsUser = false;
                loginDto.Token = string.Empty;
                return loginDto;
            }

            var isExistedUser = CheckExistedUser(request.Username, request.Password, userByName);

            if(!isExistedUser)
            {
                loginDto.IsUser = false;
                loginDto.Token = string.Empty;
                return loginDto;
            }

            var tokenString = Convert.ToBase64String(Guid.NewGuid().ToByteArray());
            var token = $"U_{tokenString}";
            userByName.Token = token;
            await _repository.SaveAsync();

            var users = await _repository.GetAllAsync<User>();
            var user = users.FirstOrDefault(x => string.Equals(token, x.Token, StringComparison.OrdinalIgnoreCase));
            if (user == null)
            {
                loginDto.IsUser = false;
                loginDto.Token = string.Empty;
                return loginDto;
            }

            loginDto.IsUser = true;
            loginDto.Token = user.Token;

            return loginDto;
        }
        private bool CheckExistedUser(string username, string password, User user)
        {
            bool isUser = true;
            var hashedPassword = _cryptoService.EncryptPassword(password.Trim(), user.Salt);

            if (!string.Equals(username.Trim(), user.Name, StringComparison.OrdinalIgnoreCase) ||
                !string.Equals(hashedPassword, user.HashedPassword, StringComparison.OrdinalIgnoreCase))
            {
                isUser = false;
                return isUser;
            }

            return isUser;
        }
    }
}
