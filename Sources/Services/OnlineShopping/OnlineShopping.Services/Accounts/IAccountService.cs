﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using OnlineShopping.API.Domains.Entities;
using OnlineShopping.API.Model.Dtos;
using OnlineShopping.API.Model.RequestCommands;
using OnlineShopping.API.Model.Validation;

namespace OnlineShopping.Services.Accounts
{
    public interface IAccountService
    {
        Task<ValidUserContext> ValidateUser(string username, string password);

        Task<UserWithRoles> GetUser(int key);
        Task<UserWithRoles> GetUser(string name);

        Task<OperationResult<RegisterDto>> CreateOnlyUser(
        CreateAccountRequest requestCommand);

        Task<OperationResult<RegisterDto>> CreateUserWithRoles(
        CreateAccountRequest requestCommand);

        Task<OperationResult<RegisterDto>> CreateUserWithRole(
        CreateAccountRequest requestCommand);

        Task<UserWithRoles> UpdateUser(
        User user, string username, string email);

        Task<bool> ChangePassword(
        string username, string oldPassword, string newPassword);

        Task<bool> AddToRole(int userKey, string role);

        Task<bool> AddToRole(string username, string role);

        Task<bool> RemoveFromRole(string username, string role);

        Task<Role> GetRole(int key);

        Task<IEnumerable<Role>> GetRoles();

        Task<LoginDto> LoginAsync(LoginRequest request);
        //Task<bool> RegisterAccountAsync(LoginRequest request);
    }
}
