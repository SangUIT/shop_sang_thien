﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using OnlineShopping.API.Model.Dtos;
using OnlineShopping.API.Model.RequestCommands;
using OnlineShopping.Services.Accounts;
using WebApiDoodle.Net.Http.Client.Model;

namespace OnlineShopping.API.Host.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class AccountsController : ControllerBase
    {
        private readonly IAccountService _accountService;
        private const string RouteName =
            "AccountsHttpRoute";
        public AccountsController(IAccountService accountService)
        {
            _accountService = accountService;
        }

        [HttpGet("id")]
        [Route("getbyid")]
        public async Task<UserWithRoles> GetAccount(int id)
        {
            var user = await _accountService.GetUser(id);

            return user;
        }

        [HttpGet]
        public ActionResult<IEnumerable<string>> Get()
        {
            return new string[] { "value1", "value2" };
        }
    }
}
