﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using OnlineShopping.API.Model.Dtos;
using OnlineShopping.Services.Clients;

namespace OnlineShopping.API.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class CustomUIController : ControllerBase
    {
        private readonly ICustomUIService _customUIService;

        public CustomUIController(ICustomUIService customUIService)
        {
            _customUIService = customUIService;
        }

        [HttpGet()]
        [Route("getall")]
        public async Task<IEnumerable<NavigationMenuDto>> GetAllAsync()
        {
            var menus = await _customUIService.GetAllMenu();

            if (!menus.Any())
            {
                return new List<NavigationMenuDto>();
            }

            return menus;
        }
    }
}