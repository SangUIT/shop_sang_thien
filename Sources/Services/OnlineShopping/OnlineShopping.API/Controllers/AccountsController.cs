﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using OnlineShopping.API.Model.Dtos;
using OnlineShopping.API.Model.RequestCommands;
using OnlineShopping.API.Model.Validation;
using OnlineShopping.Services.Accounts;

namespace OnlineShopping.API.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class AccountsController : ControllerBase
    {
        private readonly IAccountService _accountService;

        public AccountsController(IAccountService accountService)
        {
            _accountService = accountService;
        }

        [HttpGet("id")]
        [Route("getbyid")]
        public async Task<UserWithRoles> GetAccount(int id)
        {
            var user = await _accountService.GetUser(id);

            return user;
        }

        [HttpGet("name")]
        [Route("getbyname")]
        public async Task<ActionResult> GetAccountByName(string name)
        {
            var userWithRoles = await _accountService.GetUser(name);
            var user = userWithRoles.User;
            var roles = userWithRoles.Roles;
            string[] rolesArray = new string[5];

            for (int i = 0; i < roles.Count; i++)
            {
                roles.ForEach(x =>
                {
                    rolesArray[i] = x.Name;

                });
            }

            var accountDto = new AccountDto()
            {
                Name = user.Name,
                Email = user.Email,
                HashedPassword = user.HashedPassword,
                IsLocked = user.IsLocked,
                Salt = user.Salt,
                Roles = rolesArray
            };

            return Ok(accountDto as AccountDto);
        }

        [HttpPost()]
        [Route("Create")]
        public async Task<OperationResult<RegisterDto>> CreateAsync(CreateAccountRequest request)
        {
            var lengthRole = request.Roles.Length;
            var result = new OperationResult<RegisterDto>(true);

            switch (lengthRole)
            {
                case 0:
                    {
                        result = await _accountService.CreateOnlyUser(request);
                        break;
                    }
                case 1:
                    {
                        result = await _accountService.CreateUserWithRole(request);
                        break;
                    }
                default:
                    result = await _accountService.CreateUserWithRoles(request);
                    break;
            }

            return result;
        }

        [HttpPost]
        [Route("Login")]
        public async Task<LoginDto> LoginAsync(LoginRequest request)
        {
            var loginDto = await _accountService.LoginAsync(request);
            return loginDto;
        }

    }
}
