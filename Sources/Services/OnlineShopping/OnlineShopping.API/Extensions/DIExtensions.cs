﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using OnlineShopping.API.Domains.Shares;
using OnlineShopping.API.Model.RequestCommands;
using OnlineShopping.Data;
using OnlineShopping.Services.Accounts;
using OnlineShopping.Services.Clients;

namespace OnlineShopping.API.Host.Extensions
{
    public static class DIExtensions
    {
        public static void RegisterServices(this IServiceCollection services)
        {
            services.AddScoped<DbContext, OnlineShoppingContext>();

            services.AddScoped<IAccountService, AccountService>();
            services.AddScoped<IRepository, Repository>();
            services.AddScoped<ICryptoService, CryptoService>();
            services.AddScoped<ICustomUIService, CustomUIService>();

            services.AddScoped<IRequestCommand, PaginatedRequestCommand>();
        }
    }
}
