﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;

namespace OnlineShopping.Data
{
    public class Repository : IRepository
    {
        protected readonly DbContext context;

        public Repository(DbContext context)
        {
            this.context = context;
        }

        public async Task SaveAsync()
        {
            await context.SaveChangesAsync();
        }

        public async Task CreateAsync<TEntity>(TEntity entity, string createdBy) where TEntity : class
        {
            await context.Set<TEntity>()
                .AddAsync(entity);
        }

        public async Task<TEntity> CreateReturnAsync<TEntity>(TEntity entity) where TEntity : class
        {
            var result = await context.Set<TEntity>()
               .AddAsync(entity);

            return result.Entity;
        }

        public async Task DeleteAsync<TEntity>(object id) where TEntity : class
        {
            var entity = await context.Set<TEntity>()
                .FindAsync(id);
            Delete(entity);
        }

        public async Task<IEnumerable<TEntity>> GetAllAsync<TEntity>(
            Func<IQueryable<TEntity>,
                IOrderedQueryable<TEntity>> orderBy,
            string includeProperties,
            int? skip,
            int? take)
            where TEntity : class
        {
            return await GetQueryable(null, orderBy, includeProperties, skip, take)
                .ToListAsync();
        }

        public void Delete<TEntity>(TEntity entity) where TEntity : class
        {
            var dbSet = context.Set<TEntity>();
            if (context.Entry(entity)
                    .State ==
                EntityState.Detached)
            {
                dbSet.Attach(entity);
            }

            dbSet.Remove(entity);
        }
        protected virtual IQueryable<TEntity> GetQueryable<TEntity>(
            Expression<Func<TEntity, bool>> filter = null,
            Func<IQueryable<TEntity>, IOrderedQueryable<TEntity>> orderBy = null,
            string includeProperties = null,
            int? skip = null,
            int? take = null)
            where TEntity : class
        {
            includeProperties = includeProperties ?? string.Empty;
            IQueryable<TEntity> query = context.Set<TEntity>();

            if (filter != null)
            {
                query = query.Where(filter);
            }

            foreach (var includeProperty in includeProperties.Split(new[] { ',' }, StringSplitOptions.RemoveEmptyEntries))
            {
                query = query.Include(includeProperty);
            }

            if (orderBy != null)
            {
                query = orderBy(query);
            }

            if (skip.HasValue)
            {
                query = query.Skip(skip.Value);
            }

            if (take.HasValue)
            {
                query = query.Take(take.Value);
            }

            return query;
        }

        public async Task<TEntity> GetByIdAsync<TEntity>(object id) where TEntity : class
        {
            return await context.Set<TEntity>()
                .FindAsync(id);
        }
    }
}
