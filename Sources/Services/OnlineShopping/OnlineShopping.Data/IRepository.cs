﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OnlineShopping.Data
{
    public interface IRepository
    {
        Task CreateAsync<TEntity>(TEntity entity, string createdBy = null)
             where TEntity : class;

        Task<IEnumerable<TEntity>> GetAllAsync<TEntity>(
            Func<IQueryable<TEntity>, IOrderedQueryable<TEntity>> orderBy = null,
            string includeProperties = null,
            int? skip = null,
            int? take = null)
            where TEntity : class;
        Task<TEntity> GetByIdAsync<TEntity>(object id)
            where TEntity : class;

        Task<TEntity> CreateReturnAsync<TEntity>(TEntity entity)
            where TEntity : class;

        Task DeleteAsync<TEntity>(object id)
            where TEntity : class;

        Task SaveAsync();
    }
}
