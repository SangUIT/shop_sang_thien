﻿using System;
using System.Collections.Generic;
using System.Text;
using Microsoft.EntityFrameworkCore;
using OnlineShopping.API.Domains.Entities;

namespace OnlineShopping.Data
{
    public class OnlineShoppingContext : DbContext
    {
        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            if (!optionsBuilder.IsConfigured)
            {
                //optionsBuilder.UseSqlServer(@"Data Source=.\THIENNGUYEN;Initial Catalog=OnlineShoppingDev;Integrated Security=True; uid=sa; Password=123open;");
                optionsBuilder.UseSqlServer(@"Data Source=.\DESKTOP-00FGG1P;Initial Catalog=OnlineShoppingDev;Integrated Security=True");
            }
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            var Roles = new List<Role>() {
                new Role {ID = 1, Name = "Admin"},
                new Role {ID = 2, Name = "User"},
                new Role {ID = 3, Name = "Visitor"}
            };
            var userInRoles = new UserInRole() { ID = 1, RoleId = 1, UserId = 1 };

            var users = new List<User>() {
                new User {
                    ID = 1,
                    Name = "Admin",
                    HashedPassword = "WVCVrksVMi82QbRRzP0GtXKWftsJ91db5oZimO/zp6M=",
                    Salt = "Br+1huyWX0gPHT25gMFgwQ==",
                    Email = "nvthien.uit.k10@gmail.com",
                },
            };

            var navMenu = new List<NavigationMenu>() {
                new NavigationMenu(){ID = 1, Name = "Home"},
                new NavigationMenu(){ID = 2, Name = "Admin"},
            };

            modelBuilder.Entity<Role>().HasData(Roles);
            modelBuilder.Entity<User>().HasData(users);
            modelBuilder.Entity<NavigationMenu>().HasData(navMenu);
            modelBuilder.Entity<UserInRole>().HasData(userInRoles);
        }
        public DbSet<User> Users { get; set; }
        public DbSet<Role> Roles { get; set; }
        public DbSet<UserInRole> UserInRoles { get; set; }
        public DbSet<NavigationMenu> NavigationMenus { get; set; }
    }
}
