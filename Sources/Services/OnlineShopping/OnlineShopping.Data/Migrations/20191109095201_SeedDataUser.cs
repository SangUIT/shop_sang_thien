﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace OnlineShopping.Data.Migrations
{
    public partial class SeedDataUser : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "ID",
                keyValue: 1,
                columns: new[] { "HashedPassword", "Salt" },
                values: new object[] { "WVCVrksVMi82QbRRzP0GtXKWftsJ91db5oZimO/zp6M=", "Br+1huyWX0gPHT25gMFgwQ==" });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "ID",
                keyValue: 1,
                columns: new[] { "HashedPassword", "Salt" },
                values: new object[] { "jvAj6Jn6DaP/5dr6ov75Rg==", "jvAj6Jn6DaP/5dr6ov75Rg==" });
        }
    }
}
