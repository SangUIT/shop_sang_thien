﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace OnlineShopping.Data.Migrations
{
    public partial class SeedData : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.InsertData(
                table: "Users",
                columns: new[] { "ID", "CreatedOn", "Email", "HashedPassword", "IsLocked", "LastUpdatedOn", "Name", "Salt", "Token" },
                values: new object[] { 1, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), "nvthien.uit.k10@gmail.com", "jvAj6Jn6DaP/5dr6ov75Rg==", false, null, "Admin", "jvAj6Jn6DaP/5dr6ov75Rg==", null });

            migrationBuilder.InsertData(
                table: "UserInRoles",
                columns: new[] { "ID", "RoleId", "UserId" },
                values: new object[] { 1, 1, 1 });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DeleteData(
                table: "UserInRoles",
                keyColumn: "ID",
                keyValue: 1);

            migrationBuilder.DeleteData(
                table: "Users",
                keyColumn: "ID",
                keyValue: 1);
        }
    }
}
