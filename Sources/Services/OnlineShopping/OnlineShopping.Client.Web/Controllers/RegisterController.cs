﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using OnlineShopping.API.Client.Clients;
using OnlineShopping.Client.Web.ViewModels;

namespace OnlineShopping.Client.Web.Controllers
{
    public class RegisterController : Controller
    {
        private readonly IAccounts _accounts;

        public RegisterController(IAccounts accounts)
        {
            _accounts = accounts;
        }
        public IActionResult Index()
        {
            return View();
        }
        [HttpPost]
        [Route("register")]
        public async Task<IActionResult> RegisterAccout(RegisterViewModel model)
        {
            if (!ModelState.IsValid)
            {
                return View("Index");
            }

            if (!string.Equals(
                model.Password.Trim(), model.Repassword.Trim(), StringComparison.OrdinalIgnoreCase))
            {
                return View("Index");
            }

            var register = await _accounts.Register(
                model.Username, model.Password, model.Email);

            if (register == null)
            {
                return View();
            }

            return RedirectToAction("Login", "User");
        }
    }

}