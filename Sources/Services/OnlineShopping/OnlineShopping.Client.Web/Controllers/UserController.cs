﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Authentication.Cookies;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using OnlineShopping.API.Client.Clients;
using OnlineShopping.API.Domains.Shares;
using OnlineShopping.Client.Web.ViewModels;
using OnlineShopping.Services.Accounts;

namespace OnlineShopping.Client.Web.Controllers
{
    public class UserController : Controller
    {
        private readonly IAccounts _accounts;
        private readonly ICryptoService _cryptoService;
        public UserController(IAccounts accounts, ICryptoService cryptoService)
        {
            _accounts = accounts;
            _cryptoService = cryptoService;
        }
        [Route("/login")]
        [HttpGet]
        public IActionResult Login(string returnUrl = null)
        {
            ViewData["ReturnUrl"] = returnUrl;
            return View();
        }
        
        [Route("/login")]
        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> LoginAsync(string returnUrl, UserViewModel model)
        {
            var username = model.Username;
            var password = model.Password;
            if (string.IsNullOrEmpty(username) || string.IsNullOrEmpty(password))
            {
                return View();
            }

            var loginResponse = await _accounts.Login(username, password);

            if (loginResponse.IsUser)
            {
                CreateAccountCookie("_tk", loginResponse.Token, 120);
                var identity = new ClaimsIdentity(CookieAuthenticationDefaults.AuthenticationScheme);
                identity.AddClaim(new Claim(ClaimTypes.Name, model.Username.Trim()));

                var principle = new ClaimsPrincipal(identity);
                var properties = new AuthenticationProperties { IsPersistent = model.RememberMe };
                await HttpContext.SignInAsync(principle, properties);

                return RedirectToAction(returnUrl ?? "Index", "Home", new { token = loginResponse.Token ?? "" });
            }

            return View("Login");
        }
        private void CreateAccountCookie(string key, string value, int? expireTime)
        {
            var option = new CookieOptions();

            if (expireTime.HasValue)
            {
                option.Expires = DateTime.Now.AddMinutes(expireTime.Value);
            }
            else
                option.Expires = DateTime.Now.AddSeconds(10);
            Response.Cookies.Append(key, value, option);
        }
    }
}