﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;

namespace OnlineShopping.Client.Web.Controllers
{
    public class BaseController : Controller
    {
        public virtual ActionResult News()
        {
            return PartialView("Navigation_PartialView");
        }
    }
}
