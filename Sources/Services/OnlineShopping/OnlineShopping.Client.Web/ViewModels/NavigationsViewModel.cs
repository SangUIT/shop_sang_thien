﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace OnlineShopping.Client.Web.ViewModels
{
    public class NavigationsViewModel
    {
        public int ID { get; set; }
        public string Name { get; set; }
    }
}
