﻿using System;
using System.Collections.Generic;
using System.Text;
using OnlineShopping.API.Domains.Entities;

namespace OnlineShopping.API.Model.Dtos
{
    public class UserWithRoles
    {
        public User User { get; set; }
        public List<Role> Roles { get; set; }
    }
}
