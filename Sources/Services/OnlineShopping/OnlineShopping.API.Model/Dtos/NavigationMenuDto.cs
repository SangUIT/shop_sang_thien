﻿using System;
using System.Collections.Generic;
using System.Text;

namespace OnlineShopping.API.Model.Dtos
{
    public class NavigationMenuDto
    {
        public int Id { get; set; }
        public string Name { get; set; }
    }
}
