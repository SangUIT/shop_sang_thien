﻿using System;
using System.Collections.Generic;
using System.Text;

namespace OnlineShopping.API.Model.Dtos
{
    public class LoginDto
    {
        public bool IsUser { get; set; }
        public string Token { get; set; }
    }
}
