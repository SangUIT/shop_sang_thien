﻿using System;
using System.Collections.Generic;
using System.Text;
using OnlineShopping.API.Domains.Entities;

namespace OnlineShopping.API.Model.Dtos
{
    public class AccountDto
    {
        public string Name { get; set; }
        public string Email { get; set; }
        public string HashedPassword { get; set; }
        public string Salt { get; set; }
        public bool IsLocked { get; set; }
        public string[] Roles { get; set; }
    }
}
