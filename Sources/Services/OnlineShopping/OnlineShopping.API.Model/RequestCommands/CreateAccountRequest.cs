﻿using System;
using System.Collections.Generic;
using System.Text;

namespace OnlineShopping.API.Model.RequestCommands
{
    public class CreateAccountRequest : BaseCreateAccountRequest
    {
        public string[] Roles { get; set; } = new string[] { };
    }
}
