﻿using System;
using System.Collections.Generic;
using System.Text;

namespace OnlineShopping.API.Model.RequestCommands
{
    public abstract class BaseCreateAccountRequest
    {
        public string Username { get; set; }
        public string Email { get; set; }
        public string Password { get; set; }
        public string Token { get; set; }
    }
}
