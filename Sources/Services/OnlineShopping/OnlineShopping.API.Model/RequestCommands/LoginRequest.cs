﻿using System;
using System.Collections.Generic;
using System.Text;
using OnlineShopping.API.Model.Validation;

namespace OnlineShopping.API.Model.RequestCommands
{
    public class LoginRequest
    {
        public string Username { get; set; }
        public string Password { get; set; }
    }
}
