﻿using System;
using System.Collections.Generic;
using System.Security.Principal;
using System.Text;
using OnlineShopping.API.Model.Dtos;

namespace OnlineShopping.API.Model.Validation
{
    public class ValidUserContext
    {
        public IPrincipal Principal { get; set; }
        public UserWithRoles UserWithRoles { get; set; }

        public bool IsValid()
        {
            return Principal != null;
        }
    }
}
